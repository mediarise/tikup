<?php

namespace App\Listeners;

use App\Events\ApiError;
use App\Services\TelegramBotNotes;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendMessageApiError
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApiError  $event
     * @return void
     */
    public function handle(ApiError $event)
    {
        TelegramBotNotes::send($event->message);
    }
}
