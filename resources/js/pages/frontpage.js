import Vue from 'vue';
import VeeValidate from 'vee-validate';
import veeValidateConfig from '@/js/configs/veeValidate';
import index from '@/js/modules/Frontpage';

Vue.use(VeeValidate, veeValidateConfig);

new Vue({
    name: 'Frontpage',
//    store,
//    mixins: [adminSocket],
    mixins: [],
    render: h => h(index),
    el: '#v-frontpage'
});
