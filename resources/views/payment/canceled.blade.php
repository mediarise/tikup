@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card text-white bg-danger mb-3">
                    <div class="card-header">Платеж был отменён</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <p class="text-white">Если у Вас возникли трудности, пожалуйста обратитесь в поддержку <a
                                    href="support@tikup.ru">support@tikup.ru</a>
                            </p>
                            <p class="text-white"><a href="{{ route('cabinet.order') }}">Перейти в кабинет</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
