<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{

    public function index(Request $request)
    {
        return view('welcome');
    }

    public function termsOfUse(Request $request)
    {
        return view('terms-of-use');
    }

}
