<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'blogtiktok' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'blogtiktok' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 's1h4sr39' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'o9|u8K.]Dy^C@9a;/0qqJB>GYAq&P2e}AS#a.*$q9Zy*@%_n|_&;c5wei<P+9gDf' );
define( 'SECURE_AUTH_KEY',  'N/}aL6Tgnt/>Zj<[`Q}r.}kfrJv0RTO8@gm4n,zzzt(]I~p_s]Y}QR* k&xqj:@o' );
define( 'LOGGED_IN_KEY',    '/4RTwf aj1>eO6 gUaiP;Mf.ZuYy_0+*4p`Azf.!xp}{ytl|8ovoz`9XH6J/KdaK' );
define( 'NONCE_KEY',        'pf;z3Z{OgBve{jl`3|98`_Vyn(2u`R*(}x17i3lY?WzZ1h1driX_**X2^(4U]I%~' );
define( 'AUTH_SALT',        'XN4|u&{VuU6:8S/WU.T+Q7oHKPe8&$-U.^Aky56ZdsE1DV%:Ww:MFljSV?t~ zkX' );
define( 'SECURE_AUTH_SALT', 'iXHg?0-_`y!*^^CoN2:NDghU$,ul*_`xm(B>PP|KhTE=`)-`+%{d?Ux)qem]a`%d' );
define( 'LOGGED_IN_SALT',   'rQ/5N@(I:5/6~OBCNEo|(5/Jpfa]1 At24HS]deF~uXEZ4+ONy:[t0P5}aWm,WVB' );
define( 'NONCE_SALT',       '2c@IdU,&Q51EI5@HZo^LFt?r-.)YZCKsr R687TJX| _wx.QZURqsLg@,*H2Pu[9' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'tb_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
