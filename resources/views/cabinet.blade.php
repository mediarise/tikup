@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Мой кабинет</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <!--
                        <div class="alert alert-success" role="alert">
                           Вы можите использовать свой ваучер, для накрутки 15 пользователей, лайков или просмотров
                            <form action="/action_page.php">
                                <div class="form-group">
                                    <label for="voucher">Введите Ваш ваучер, если он у Вас есть:</label>
                                    <input type="text" class="form-control" id="voucher">
                                </div>

                                <button type="submit" class="btn btn-primary">Активировать</button>
                            </form>
                        </div>
                        -->
                        <a href="{{ route('cabinet.order') }}" class="btn btn-primary">Перейти к заказам</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
