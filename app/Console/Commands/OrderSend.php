<?php

namespace App\Console\Commands;

use App\Invoice;
use App\Mail\OrderNotPaidMailable;
use App\Mail\OrderSuccessMailable;
use App\Order;
use App\OrderLogs;
use App\Price;
use App\Services\TikTopApi;
use Auth;
use Illuminate\Console\Command;
use DB;
use Mail;

class OrderSend extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запускает оправку Email по заказам';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $orders = Order::with('invoice')->where(['status' => Order::STATUS_COMPLETED, 'send_success' => Order::SEND_SUCCESS_NOT_SENDED])->limit(100)->distinct()->get()->all();

        if (!empty($orders)) {
            array_map(function (Order $order) {
                $name = $order->user->name;
                $orderId = $order->id;

                Mail::to($order->user->email)
                    ->send(new OrderSuccessMailable($name, $orderId));

                $order->send_success = Order::SEND_SUCCESS_SENDED;
                $order->send_success_at = date('Y-m-d H:i:s');
                $order->save();
                OrderLogs::log('Заказ номер: ' . $order->id . ' выполнен, отправлено письмо на email: ' . $order->user->email);

            }, $orders);
        }

        return 0;

        $nowTime = date('Y-m-d H:i:s', strtotime('-1 hours'));
        $nowTimeDeletedTwoHour = date('Y-m-d H:i:s', strtotime('-3 hours'));

        $orders = Order::with('invoice')
            ->where([
                'status' => Order::STATUS_NEW,
                'send_not_pay' => Order::SEND_NOT_PAY_NOT_SENDED
            ])
            ->whereBetween('created_at', [$nowTimeDeletedTwoHour, $nowTime])
            ->limit(100)->distinct()->get()->all();

        if (!empty($orders)) {
            array_map(function (Order $order) {
                $name = $order->user->name;
                Mail::to($order->user->email)->send(new OrderNotPaidMailable($name));

                $order->send_not_pay = Order::SEND_NOT_PAY_SENDED;
                $order->send_not_pay_at = date('Y-m-d H:i:s');
                $order->save();
                OrderLogs::log('Заказ номер: ' . $order->id . ' не оплачен (в течении 2х часов), отправлено письмо на email: ' . $order->user->email);
            }, $orders);
        }

        $orders = Order::with('invoice')
            ->where(['status' => Order::STATUS_FAIL_PAID, 'send_not_pay' => Order::SEND_NOT_PAY_NOT_SENDED])
            ->limit(100)->distinct()->get()->all();

        if (!empty($orders)) {
            array_map(function (Order $order) {
                $name = $order->user->name;
                Mail::to($order->user->email)->send(new OrderNotPaidMailable($name));

                $order->send_not_pay = Order::SEND_NOT_PAY_SENDED;
                $order->send_not_pay_at = date('Y-m-d H:i:s');
                $order->save();
                OrderLogs::log('Заказ номер: ' . $order->id . ' не оплачен (ошибка оплаты), отправлено письмо на email: ' . $order->user->email);

            }, $orders);
        }
        return 0;
    }
}
