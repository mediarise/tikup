<?php

namespace App\Console\Commands;

use App\Order;
use App\OrderLogs;
use App\Services\TikTopApi;
use Illuminate\Console\Command;

class OrderStatus extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запускает обработку оплаченных заказов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::with('invoice')
            ->where('status', Order::STATUS_PENDING)
            ->orWhere('status', Order::STATUS_IN_PROGRESS)
            ->limit(100)->distinct()->get()->all();

        if (empty($orders)) {
            return 1;
        }

        $ordersId = [];
        foreach ($orders as $order) {
            $ordersId[] = $order->order_id_api;
        }

        try {
            $tikTopApi = new TikTopApi();
            $statusOrders = json_decode(json_decode($tikTopApi->multiStatus($ordersId)->getContent()), true);

            array_map(function (Order $order) use ($statusOrders) {
                if (isset($statusOrders[$order->order_id_api]) && $statusOrders[$order->order_id_api]['status'] === 'completed') {
                    $order->status = Order::STATUS_COMPLETED;
                    $order->charge = (int)$statusOrders[$order->order_id_api]['remains'];
                    OrderLogs::log('Заказ номер: ' . $order->id . ' выполнен');
                    $order->save();
                } elseif (isset($statusOrders[$order->order_id_api]) && $statusOrders[$order->order_id_api]['status'] === 'in_progress') {
                    $order->status = Order::STATUS_IN_PROGRESS;
                    $order->charge = (int)$statusOrders[$order->order_id_api]['remains'];
                    //  OrderLogs::log('Заказ номер: ' . $order->id . ' выполняется');
                    $order->save();
                } elseif (isset($statusOrders[$order->order_id_api]) && $statusOrders[$order->order_id_api]['status'] === 'pending') {
                    $order->status = Order::STATUS_PENDING;
                    $order->charge = (int)$statusOrders[$order->order_id_api]['remains'];
                    OrderLogs::log('Заказ номер: ' . $order->id . ' ожидает выполнение');
                    $order->save();
                } elseif (isset($statusOrders[$order->order_id_api]) && $statusOrders[$order->order_id_api]['status'] === 'rejected') {
                    $order->status = Order::STATUS_REJECTED;
                    OrderLogs::log('Заказ номер: ' . $order->id . ' отменен системой API');
                    $order->save();
                }
            }, $orders);
            return 0;
        } catch (\Exception $e) {
            return 2;
        }
    }
}
