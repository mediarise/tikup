<?php

namespace App\Services;

use Yii;

class TikTopApi
{
    const APP_URL = 'https://tiktop-free.com/api/v2';

    const SERVICE_LIKES = 1;
    const SERVICE_FOLLOWERS = 2;
    const SERVICE_VIEWS = 5;

    private $apiKey;

    /**
     * TikTopApi constructor.
     */
    function __construct()
    {
        $config = app('config')->get('services');
        $this->apiKey = $config['tiktop_api_token'] ?? null;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function order($data)
    {
        $post = array_merge($data, [
            'action' => 'add'
        ]);

        return response()->json($this->connect($post), 200);
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function status($order_id)
    {

        return response()->json($this->connect([
            'action' => 'status',
            'order' => $order_id
        ]), 200);
    }

    /**
     * @param $order_ids
     * @return mixed
     */
    public function multiStatus($order_ids)
    {
        return response()->json($this->connect([
            'action' => 'status',
            'orders' => implode(",", (array)$order_ids)
        ]), 200);

    }

    /**
     * @return mixed
     */
    public function services()
    {
        return response()->json($this->connect([
            'action' => 'services',
        ]), 200);
    }

    /**
     * @param null $currency
     * @return mixed
     */
    public function balance($currency = null)
    {
        $data = array(
            'action' => 'balance',
        );

        if (is_string($currency) && trim(strval($currency)) !== '') {
            $data['currency'] = strval($currency);
        }

        return response()->json($this->connect($data), 200);
    }

    /**
     * @param $data
     * @return bool|string
     */
    private function connect($data)
    {
        $post = Array();
        if (is_array($data)) {
            $data['key'] = $this->apiKey ?? null;
            foreach ($data as $name => $value) {
                $post[] = $name . '=' . urlencode($value);
            }
        }

        $ch = curl_init(self::APP_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        if (is_array($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, join('&', $post));
        }
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        $result = curl_exec($ch);
        if (curl_errno($ch) != 0 && empty($result)) {
            $result = false;
        }
        curl_close($ch);
        return $result;
    }
}

