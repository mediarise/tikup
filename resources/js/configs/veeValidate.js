import en from 'vee-validate/dist/locale/en';
import ru from 'vee-validate/dist/locale/ru';
export default {
    locale: 'ru',
    dictionary: {
        en: en,
        ru: {
            messages: {
                ...ru.messages,
                required: () => 'Поле обязательно для заполнения',
                min: (fieldName, params, data) => `В поле должно быть минимум ${params} символов`,
                regex: (fieldName, params, data) => {
                    console.log(window.form_type)

                    if (window.form_type === 'followers') {
                        return `Введите корректный логин для накрутки подписчиков. Например: @username`
                    }

                    return `Введите корректную ссылку. Например: https://vm.tiktok.com/2HD3N5S/ или https://www.tiktok.com/@username/video/6844290493996698886`
                }
            }
        }
    },
    events: 'input|blur|on-close'
};
