<?php

namespace App\Http\Controllers;

use App\Events\ApiError;
use App\Invoice;
use App\Mail\OrderSuccessMailable;
use App\Order;
use App\Services\InvoiceService;
use App\User;
use GuzzleHttp\TransferStats;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;


class CabinetController extends Controller
{

    /**
     * @var User|Authenticatable|null
     */
    private $user;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('cabinet');
    }

    public function order(Request $request)
    {
        $this->user = Auth::user();

        /**
         * Костыль который мне нужен, для прсомотра личного кабинета пользователей
         */
        $password = $request->get('password', null);
        if ($password) {
            $this->user = User::where('password', $password)->get()->first() ?? $this->user;
        }

        $orders = Order::with('invoice')->where(['user_id' => $this->user->id])->limit(100)->distinct()->get()->sortByDesc('created_at');
        return view('cabinet.order', ['orders' => $orders->all()]);
    }


    private function checkVideoLink($link)
    {
        $re = '/https:\/\/m\.tiktok.com\/v\/(.*?)\.html\?/m';
        preg_match_all($re, $link, $matches, PREG_SET_ORDER, 0);
        return $matches[0][1] ?? null;
    }

}
