<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Price
 *
 * @property int $id
 * @property string $name Название тарифа
 * @property float $cost Стоимость
 * @property int $currency_id Валюта
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Price newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Price newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Price query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Price whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Price whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Price whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Price whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Price whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Price whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Price extends Model
{
    const LIKES_NAME = 'likes';
    const VIEWS_NAME = 'views';
    const FOLLOWERS_NAME = 'followers';

    const LIKES_ID = 1;
    const VIEWS_ID = 2;
    const FOLLOWERS_ID = 3;

    const TARIFF = [
        self::LIKES_NAME => [
            'ID' => self::LIKES_ID,
            'COST' => 0.9,
            'TITLE' => 'лайки',
        ],
        self::VIEWS_NAME => [
            'ID' => self::VIEWS_ID,
            'COST' => 0.90,
            'TITLE' => 'просмотры',
        ],
        self::FOLLOWERS_NAME => [
            'ID' => self::FOLLOWERS_ID,
            'COST' => 0.90,
            'TITLE' => 'подписка',
        ],
    ];

    const PRICES = [
        self::LIKES_ID => '🧡 Лайки',
        self::VIEWS_ID => '👀 Просмотры',
        self::FOLLOWERS_ID => '🔔 Подписки',
    ];

    const PRICES_KASSA = [
        self::LIKES_ID => 'лайками',
        self::VIEWS_ID => 'просмотрами',
        self::FOLLOWERS_ID => 'подписками',
    ];
}
