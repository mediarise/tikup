<?php

namespace App;

use App\Services\payments\Provider;
use App\Services\payments\YandexKassaProvider;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Invoice
 *
 * @property int $id
 * @property int $user_id
 * @property float $total Всего
 * @property int $price_id ID прайса
 * @property int $value Количество заказа
 * @property string|null $payment_at Дата и время платежа
 * @property int $status Статус платежа
 * @property string $payment_id ID Платежа в платежной системе
 * @property string $description Описание платежа
 * @property int $operator_pay_id Платежная система
 * @property mixed $tax_response Ответ системы чеков
 * @property int $currency_id Валюта
 * @property string $idempodence_key Ключ идемпотентности
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read $paymentUrl Ссылка на оплату
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereIdempodenceKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereOperatorPayId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice wherePaymentAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice wherePaymentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice wherePriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereTaxResponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereValue($value)
 * @mixin \Eloquent
 */
class Invoice extends Model implements InvoiceInterface
{
    const STATUS_NEW = 1;
    const STATUS_PENDING_PAY = 2;
    const STATUS_PAID = 3;
    const STATUS_FAIL = 0;
    const STATUS_WAITING_CAPTURE = 4;
    const STATUS_CANCELED = 5;

    const CURRENCY_RUB = 1;
    const CURRENCY_USD = 2;

    const CURRENCY_LIST_CODES = [
        self::CURRENCY_RUB => 'RUB',
        self::CURRENCY_USD => 'USD',
    ];

    const YANDEX_KASSA_ID = 1;

    const SALT = 'aw2s32fxpb';

    public $fillable = ['status', 'total', 'value'];

    public function applyPayment()
    {
        try {

            if ((int)$this->status === Invoice::STATUS_PAID) {

                return $this->status;
            }
            $order = Order::where('invoice_id', '=', $this->id)->firstOrFail();


            $this->status = Invoice::STATUS_PAID;
            $this->payment_at = date('Y-m-d H:i:s');

            OrderLogs::log('оплатил заказ на сумму ' . $this->total
                . ' тип услуги: ' . Price::PRICES[$this->price_id] . ' ссылка ' . $order->link);

            if ($this->save()) {
                OrderLogs::log('заказ присвоен статус оплачен');
            } else {
                OrderLogs::log('Ошибка: заказ не присвоен статус оплачен');
            }


            if ((int)$order->status === Order::STATUS_PENDING_PAY) {

                $order->status = Order::STATUS_PAID;

                if ($order->save()) {
                    OrderLogs::log('заказ присвоен статус оплачен');
                } else {
                    OrderLogs::log('Ошибка: заказ не присвоен статус оплачен');
                }
                OrderLogs::log('Оплата пользователем произведена');
            } else {
                OrderLogs::log('ЭТОТ ЗАКАЗ БЫЛ УЖЕ В РАБОТЕ С АПИ: №: ' . $order->id . ' СТАТУС: ' . $order->status);
            }


            return $this->status;

        } catch (Exception $e) {
            OrderLogs::log('Ошибка с заказом у пользователя, invoice = ' . $this->id . ' ' . $e->getMessage());
            return Invoice::STATUS_FAIL;
        }
    }

    public function pendingPayment()
    {
        if ((int)$this->status === Invoice::STATUS_PENDING_PAY || (int)$this->status === Invoice::STATUS_WAITING_CAPTURE) {
            return $this->status;
        }

        $this->status = Invoice::STATUS_PENDING_PAY;
        $this->save();

        $order = Order::where('invoice_id', '=', $this->id)->firstOrFail();
        $order->status = Order::STATUS_PENDING_PAY;
        $order->save();

        OrderLogs::log('Ожидание оплаты у пользователя');
    }

    public function canceledPayment()
    {
        if ((int)$this->status === Invoice::STATUS_CANCELED) {
            return $this->status;
        }

        $this->status = Invoice::STATUS_CANCELED;
        $this->save();

        $order = Order::where('invoice_id', '=', $this->id)->firstOrFail();
        $order->status = Order::STATUS_FAIL_PAID;
        $order->save();
        OrderLogs::log('Пользователь отменил оплату');
    }

    public function failPayment()
    {
        if ((int)$this->status === Invoice::STATUS_FAIL) {
            return $this->status;
        }

        $this->status = Invoice::STATUS_FAIL;
        $this->save();

        $order = Order::where('invoice_id', '=', $this->id)->firstOrFail();
        $order->status = Order::STATUS_FAIL_PAID;
        $order->save();
        OrderLogs::log('Ошибка оплаты у пользователя');
    }

    public function refundPayment()
    {
        OrderLogs::log('Пользователь пытался совершить возврат');
    }

    public function getStatusPay()
    {
        if (empty($this->operator_pay_id)) {
            throw new \Exception('Неуказан оператор оплаты');
        }

        switch ($this->operator_pay_id) {
            case static::YANDEX_KASSA_ID:
                $provider = new YandexKassaProvider();
                $provider->paymentId = $this->payment_id;
                return $provider->getPaymentStatus();
                break;
        }

        throw new \Exception('Неуказан оператор оплаты');
    }

    public function getPaymentUrl()
    {
        if (empty($this->operator_pay_id)) {
            throw new \Exception('Неуказан оператор оплаты');
        }

        switch ($this->operator_pay_id) {
            case static::YANDEX_KASSA_ID:

                $provider = new YandexKassaProvider();
                $provider->currency = static::CURRENCY_LIST_CODES[$this->currency_id];
                $provider->capture = true;
                $provider->return_url = $this->getConfirmPaymentUrl();
                $provider->description = $this->description;
                $provider->idempotenceKey = $this->idempodence_key;
                $provider->sum = $this->total;
                $provider->type_service = Price::PRICES_KASSA[$this->price_id];
                $provider->count = $this->value;

                $confirmation = $provider->getPaymentConfirmation();
                $this->payment_id = $confirmation->paymentId;
                $this->save();
                return $confirmation->confirmationUrl;
                break;
        }

        throw new \Exception('Неуказан оператор оплаты');
    }

    public function getConfirmPaymentUrl()
    {
        return route('payment.confirm', ['invoice_id' => $this->id, 'code' => $this->getValidationCode()]);
    }

    public function validatePayment($code)
    {
        return $this->getValidationCode() === $code;
    }

    public function getValidationCode()
    {
        return md5(static::SALT . ':' . $this->id);
    }

    /**
     *
     * @throws Exception
     */
    public function getPaymentProvider(): Provider
    {
        $paymentProvider = null;

        if (empty($this->operator_pay_id)) {
            throw new Exception('Оператор пустой');
        }

        switch ($this->operator_pay_id) {
            case static::YANDEX_KASSA_ID:
                $paymentProvider = new YandexKassaProvider();
                $paymentProvider->idempotenceKey = $this->idempodence_key;
                $paymentProvider->paymentId = $this->payment_id;
                break;
        }

        return $paymentProvider;
    }

}
