<?php

return [
    'provider' => [
        'yandexKassa' =>
            [
                'shopId' => env('YANDEX_KASSA_SHOP_ID'),
                'secret' => env('YANDEX_KASSA_SECRET'),
            ]
    ]
];
