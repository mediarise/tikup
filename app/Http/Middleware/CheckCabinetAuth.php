<?php

namespace App\Http\Middleware;

use яApp\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use Route;

class CheckCabinetAuth
{

    public function handle($request, Closure $next, $guard = null)
    {
        Route::get('cabinet', function () {
            // Только для пользователей с подтвержденным адресом электронной почты...
        })->middleware('verified');

        return $next($request);
    }

}
