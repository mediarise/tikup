<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderNotPaidMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $name;

    /**
     * Create a new message instance.
     *
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->subject = "Вам нужна помощь?";
        $this->from = [
            ['name' => 'TikUp.ru',
                'address' => 'noreply@tikup.ru']
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.order.notpaid');
    }
}
