<?php

namespace App\Console\Commands;

use App\Invoice;
use App\Order;
use App\Price;
use App\Services\TikTopApi;
use Illuminate\Console\Command;
use DB;

class OrderImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Добавляет картинки, если их нет в таблице Order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $orders = Order::with('invoice')->where(['image' => null])->limit(100)->distinct()->get()->all();


        if (empty($orders)) {
            return 1;
        }

        foreach ($orders as $order) {
            $order->image = $order->getImage($order->link);
            $order->save();
        }
        return 0;
    }
}
