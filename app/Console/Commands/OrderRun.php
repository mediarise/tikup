<?php

namespace App\Console\Commands;

use App\Events\ApiError;
use App\Order;
use App\OrderLogs;
use App\Price;
use App\Services\TikTopApi;
use Illuminate\Console\Command;

class OrderRun extends Command
{

    private static $mapService = [
        Price::LIKES_ID => TikTopApi::SERVICE_LIKES,
        Price::FOLLOWERS_ID => TikTopApi::SERVICE_FOLLOWERS,
        Price::VIEWS_ID => TikTopApi::SERVICE_VIEWS,
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запускает обработку оплаченных заказов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $orders = Order::with('invoice')->where(['status' => Order::STATUS_PAID])->limit(100)->distinct()->get()->all();


        if (empty($orders)) {
            return 1;
        }

        $tikTopApi = new TikTopApi();
        foreach ($orders as $order) {
            $data = [];
            $data['service'] = self::$mapService[$order->invoice->price_id];
            $data['link'] = $order->link;
            $data['quantity'] = $order->invoice->value;
            $result = $tikTopApi->order($data);

            /**
             * hahahah, cry :(
             */
            $content = json_decode(json_decode($result->getContent(true)), true);

            OrderLogs::log('Ответ сервера API: ' . json_decode($result->getContent(true)));


            $order->status = Order::STATUS_ERROR;
            $contentJson = json_encode($content);

            if (isset($content['order'])) {
                $order->order_id_api = $content['order'];
                $order->status = Order::STATUS_PENDING;
                OrderLogs::log('Заказ номер: ' . $order->id . ' ожидает обработки');
            } else {
                $order->status = Order::STATUS_REJECTED;
                OrderLogs::log('Заказ номер: ' . $order->id . ' отменен сервисом API');

                event(new ApiError("Ошибка API: Order Id: {$order->id} Invoice Id: {$order->invoice->id} Msg: {$contentJson} User Id: {$order->user->id} User Hash: {$order->user->password}"));
            }

            if (isset($content['error']) || $order->status === Order::STATUS_ERROR) {
                OrderLogs::log("Ошибка при обращении к TikTopApi:" . $content['error'] ?? '');
                event(new ApiError("Ошибка API: Order Id: {$order->id} Invoice Id: {$order->invoice->id} Msg: {$contentJson} User Id: {$order->user->id} User Hash: {$order->user->password}"));
            }
            $order->save();
        }
        return 0;
    }
}
