<?php

namespace App\Services\payments;

class Atol
{
    public $api_version = '';
    public $login = '';
    public $pass = '';
    public $group_code = '';
    public $inn = '';
    public $token = '';

    function Send($url, $params = [], $useContentTypeHeader = true)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($params) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params, JSON_UNESCAPED_UNICODE));
        }
        $headers = [];

        if ($useContentTypeHeader) {
            $headers[] = 'Content-type: application/json; charset=utf-8';
        }

        if (!empty($this->token)) {
            $headers[] = 'Token: ' . $this->token;
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        //curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8888');
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $res = curl_exec($ch);

        curl_close($ch);
        return json_decode($res, true);
    }

    public function getToken()
    {
        $this->token = '';
        $rez = $this->Send('https://online.atol.ru/possystem/' . $this->api_version . '/getToken', ['login' => $this->login, 'pass' => $this->pass]);
        $this->token = $rez['token'];
        return $rez;
    }

    /*
    $operation - Тип документа
    o sell: чек «Приход»;
    o sell_refund: чек «Возврат прихода»;
    o sell_correction: чек «Коррекция прихода»;
    o buy: чек «Расход»;
    o buy_refund: чек «Возврат расхода»;
    o buy_correction: чек «Коррекция расхода».

    $params - параметры документа
    */
    public function sendDoc($operation, $params)
    {
        $rez = $this->Send('https://online.atol.ru/possystem/' . $this->api_version . '/' . $this->group_code . '/' . $operation . '?token=' . $this->token, $params);
        return $rez;
    }

    public function checkDoc($uuid)
    {
        $rez = $this->Send('https://online.atol.ru/possystem/' . $this->api_version . '/' . $this->group_code . '/report/' . $uuid . '?token=' . $this->token);
        return $rez;
    }

    public function createRequestFromInvoice(Invoice $invoice)
    {
        $customerEmail = $invoice->getDataValue('email', $invoice->user->mail);

        return [
            'external_id' => strval($invoice->id),
            'receipt' => [
                'client' => [
                    'email' => $customerEmail,
                ],
                'company' => [
                    'email' => 'support@zengram.ru',
                    'sno' => 'usn_income',
                    'inn' => $this->inn,
                    'payment_address' => 'zengram.ru',
                ],
                'items' => [
                    [
                        'name' => 'Пополнение баланса',
                        'price' => $invoice->sum,
                        'quantity' => 1,
                        'sum' => $invoice->sum,
                        'payment_method' => 'full_prepayment',
                        'payment_object' => 'service',
                        'vat' => [
                            'type' => 'none'
                        ]
                    ]
                ],
                'payments' => [
                    [
                        'type' => 1,
                        'sum' => $invoice->sum
                    ]
                ],
                'total' => $invoice->sum,
            ],
            'service' => [
                'callback_url ' => Url::to(['/payment/atol-callback'], true),
            ],
            'timestamp' => date('d.m.Y H:i:s'),
        ];
    }
}
