<?php

namespace App\Services\payments;

use App\PaymentNotificationInterface;

interface Provider
{
    public function request($method, $endpointUrl, $credentials, $data, $headers);

    public function getPaymentConfirmationObject(): PaymentConfirmation;

    public function handleRequest(PaymentNotificationInterface $notify): array;

    public function getPaymentStatus(): int;
}
