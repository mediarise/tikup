@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Пользовательское соглашение</div>

                    <div class="card-body" style="text-align: justify">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">Настоящая пользовательское соглашение (далее по тексту &ndash; &laquo;Соглашение&raquo;) является официальным предложением Исполнителя заключить с любым физическим лицом, далее именуемым &ndash; &laquo;Пользователь&raquo;, Договор возмездного оказания услуг на условиях, определенных в тексте Соглашения. Акцепт настоящего Соглашения осуществляется путём оформления Пользователем заказа на сервисе. Если Пользователь оформляет заказ на сервисе, то принимает и соглашается с условиями и правилами Соглашения. Акцепт Соглашения означает ознакомление и полное, безусловное и безоговорочное согласие Пользователя с условиями и требованиями, определёнными в Соглашении. С момента акцепта Соглашения Договор на оказание информационных Услуг между Исполнителем и Пользователем признается заключенным и согласованным между ними, а его условия подлежат обязательному исполнению Сторонами.</span></span></span></span></p>

                        <p><span ><strong><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">ТЕРМИНЫ</span></span></span></strong></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">В целях настоящего Соглашения нижеприведенные термины используются в следующем значении:</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">1.1. Сервис &ndash; веб-сайт, принадлежащий Исполнителю, расположенный по адресу https://</span></span></span><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">tikup</span></span></span><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">.</span></span></span><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">ru</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">1.2. Исполнитель &mdash; сервис, в лице владельца сайта.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">1.3. Пользователь &ndash; физическое лицо, самостоятельно пользующееся (воспользовавшееся) функциями и услугами, предоставляемыми сервисом https://tik</span></span></span><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">up</span></span></span><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">.ru.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">1.4. Заказ &ndash; заявка Пользователя на получение выполнение функции сервиса, определенных в тексте настоящего Соглашения.</span></span></span></span></p>

                        <p><span ><strong><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">ПРЕДМЕТ СОГЛАШЕНИЯ</span></span></span></strong></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">2.1. В рамках настоящего Соглашения Исполнитель по Заказу Пользователя обязуется оказывать Пользователю услуги по предоставлению в пользование Сайта для оформления заказов Пользователя а также для получения связанных с этой информацией дополнительных услуг, а Пользователь обязуется оплачивать данные Услуги в размере, на условиях и в порядке, установленных Соглашением. Оплата заказов происходит со счета Пользователя. Выводить средства, по причинам не предусмотренных настоящим соглашением запрещено.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">2.2. Перечень и стоимость Услуг Исполнителя публикуются на Сайте.</span></span></span></span></p>

                        <p><span ><strong><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">РАСЧЕТЫ МЕЖДУ СТОРОНАМИ</span></span></span></strong></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">3.1. Оплата Услуг Исполнителя осуществляется безналичным способом путем перечисления денежных средств на расчетный счет Исполнителя. Денежная сумма, перечисленная на электронный счет Исполнителя, о чем Пользователь получает уведомление на свой почтовый ящик, который был им указан при оформлении заказа.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">3.2. Датой оплаты признается дата поступления средств на электронный счет Исполнителя.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">3.3. Использование денежных средств возможны исключительно на оплату услуг и функций сервиса. Вывод на электронный кошелек или любую другую платежную, перевод другому пользователю невозможен.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">3.4. Оплата производится путем платежей через онлайн агрегаторы и электронные кошельки. Данные средства являются добровольными пожертвованиями и идут исключительно на развитие проекта.</span></span></span></span></p>

                        <p><span ><strong><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">ФАКТ ОКАЗАНИЯ УСЛУГ</span></span></span></strong></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">4.1. Факт оказания Услуг по настоящему Соглашению считается достижением заказанного количества подписчиков/просмотров/лайков/друзей/репостов/голосов на счетчике группы/видео/канала/фото/поста/голосования. Все результаты фиксируются исключительно по счетчикам в социальных сетях.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">4.2. В процессе выполнения заказа использование любых сторонних сервисов и способов привлечения людей запрещено, ввиду того, что большинство услуг при подсчете количества выполнений опираются на внутреннюю статистику группы/видео/страницы/канала/поста/голосования.</span></span></span></span></p>

                        <p><span ><strong><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">ОТВЕТСТВЕННОСТЬ СТОРОН</span></span></span></strong></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">5.1. При неисполнении или ненадлежащем исполнении обязательств Стороны несут ответственность, предусмотренную действующим законодательством Российской Федерации.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">5.2. В случае не оплаты Пользователем Услуг Исполнитель вправе приостановить оказание Услуг Пользователю и ограничить доступ Пользователя к Сайту.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">5.3. Пользователь сервиса не имеет права принудительно, путем угроз, шантажа или вымогательства требовать сделать перевод или возврат средств которые были потрачены на функции сервиса.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">5.4. Стороны освобождаются от ответственности за нарушение условий настоящего Договора, если такое нарушение вызвано действием форс-мажорных обстоятельств (обстоятельств непреодолимой силы). Стороны договорились, что такими действиями, в частности являются действия органов государственной власти, местного самоуправления, пожар, наводнение, землетрясение, другие стихийные действия, отсутствие электроэнергии и/или сбои работы компьютерной сети, забастовки, гражданские волнения, беспорядки, незапланированное изменения алгоритмов администрацией социальных сетей. В случае возникновения форс-мажорных обстоятельств, установленные сроки по выполнению обязательств, указанные в Соглашение, переносятся на срок, в течение которого действуют возникшие обстоятельства.</span></span></span></span></p>

                        <p><span ><strong><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">РАЗРЕШЕНИЕ СПОРОВ</span></span></span></strong></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">6.1. Стороны установили претенциозный до судебный порядок урегулирования разногласий и споров. Срок для ответа на предъявленную претензию составляет 30 (тридцать) рабочих дней с момента ее получения Стороной.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">6.2. Только техническая поддержка, оператор сервиса имеет право вынести исключительно верное решение спора. Исполнитель или его представитель, оператор технической поддержки вправе отказать в решение проблемы или жалобы пользователя при наличии за ним нарушений.</span></span></span></span></p>

                        <p><span ><strong><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">ПРОЧИЕ УСЛОВИЯ</span></span></span></strong></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">7.1. Для исполнения своих обязательств по настоящему Соглашению Исполнитель вправе привлекать третьих лиц.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">7.2. Настоящим Пользователь дает согласие на хранение, обработку и передачу, в том числе передачу третьей стороне, данных о своих заказах.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">7.3. Использование персональных данных и иной информации Пользователя, в том числе предоставленной при оформлении заказа на Сайте, по настоящему Соглашению осуществляется исключительно в целях исполнения Исполнителем своих обязательств.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">7.4. Настоящим Пользователь дает согласие на получение от Исполнителя различных служебных и информационных сообщений посредством почты, телефонной (в том числе мобильной) связи, электронной почты и иных форм и каналов направления/получения информации.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">7.5. Сервис не несет ответственности за утерю или кражу логина и пароля от аккаунта в социальной сети Пользователя.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">7.6. Гарантии от любых защитных действий социальных сетей против массовой раскрутки не даются, средства не возвращаются.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">7.7. Если в ходе выполнения гарантийной услуги или после её выполнения возникли списания просмотров/подписичков/фолловеров сервис обязан их восстановить в срок 40 дней с момента обращения заказчика в техническую поддержку. Возврат средств по гарантии не предусмотрен.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">7.8. Сервис снимает с себя все гарантийные обязательства, если Пользователь использовал до или после обращения к Исполнителю для накрутки сторонние сервисы, сайты, прочих исполнителей, а также если использовал разные тарифы накрутки в одну группу/страницу.</span></span></span></span></p>

                        <p><span ><strong><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">ДЕЙСТВИЕ ПОЛЬЗОВАТЕЛЬСКОГО СОГЛАШЕНИЯ</span></span></span></strong></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">8.1. Настоящее Соглашение вступает в силу с момента ее размещения на Сайте.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">8.2. Настоящее Соглашение размещено на неопределенный срок и утрачивает свою силу при ее аннулировании Исполнителем.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">8.3. В случае внесения изменений в Соглашение, такие изменения вступают в силу с момента опубликования новой редакции Соглашения на Сайте, если иной срок вступления изменений в силу не определен дополнительно при их публикации. Исполнитель вправе в одностороннем порядке осуществлять внесение изменений в текст Соглашения.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">8.4. Пользователь обязуется самостоятельно осуществлять контроль за изменениями в положениях настоящего Соглашения и нести ответственность за последствия, связанные с несоблюдением данной обязанности.</span></span></span></span></p>

                        <p><span ><span style="font-size:10.5pt"><span style="font-family:&quot;Open Sans&quot;,sans-serif"><span style="color:black">8.5. При несогласии Пользователя с соответствующими изменениями Пользователь обязан прекратить использование Сайта и отказаться от Услуг Исполнителя. В противном случае продолжение использования Пользователем Сайта означает, что Пользователь согласен с условиями Соглашения в новой редакции.</span></span></span></span></p>

                        <p>&nbsp;</p>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
