<?php

namespace App\Services;

use App\OrderLogs;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\TransferStats;

/**
 * Class PrepareLink
 * @package App\Services
 */
class PrepareLink
{
    const AVATAR_REGULAR = '/.*?tiktok-avatar tiktok-avatar-circle.*?src="(.*?)"\/>.*?/m';
    const VM_LINK_REDIRECT = '/https:\/\/m\.tiktok.com\/v\/(.*?)\.html\?/m';
    const VM_LINK = '/.*?(vm\.tiktok\.com).*?/m';

    private $link;
    private $client;

    public function __construct(String $link, ClientInterface $client)
    {
        $this->link = $link;
        $this->client = $client;
    }

    /**
     * @return |null
     */
    public function getAvatar()
    {
        try {
            $data = $this->client->request('GET', $this->link, [
                'headers' => ['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3835.0 Safari/537.36']
            ])->getBody()->getContents();

            preg_match_all(self::AVATAR_REGULAR, $data, $matches, PREG_SET_ORDER, 0);

            $found = $matches[0][1];
            $result =  trim(str_replace('&amp;', '&', $found));
            return $result;
        } catch (\Throwable $e) {
            dd($e->getMessage());
            OrderLogs::log('не удалось получить аватар по ссылке ' . $this->link);
            return null;

        }
    }

    /**
     * @return |null
     */
    public function getVideoPreview()
    {
        preg_match_all(self::VM_LINK, $this->link, $matches, PREG_SET_ORDER, 0);
        if (!empty($matches)) {
            $this->link = $this->getVmLink($this->link);
        }
        return $this->getThumb($this->link);
    }

    /**
     * @param $vm
     * @return string
     */
    private function getVmLink($vm)
    {
        $this->client->get($vm, [
            //'query' => ['get' => 'params'],
            'on_stats' => function (TransferStats $stats) use (&$link) {
                $url = $stats->getEffectiveUri();
                $link = $link ?: $this->checkVideoLink($url);
            }
        ])->getBody()->getContents();
        return "https://m.tiktok.com/v/$link.html";
    }

    /**
     * @param $link
     * @return string|null
     */
    private function getThumb($link)
    {
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://www.tiktok.com/oembed?url=' . $link);
            $data = $response->getBody()->getContents();
            $dataJson = json_decode($data);
            return $dataJson->thumbnail_url ?? null;
        } catch (\Throwable $e) {
            OrderLogs::log('не удалось получить картинку по ссылке ' . $link);
            return null;
        }
    }

    /**
     * @param $link
     * @return string|null
     */
    private function checkVideoLink($link)
    {
        preg_match_all(self::VM_LINK_REDIRECT, $link, $matches, PREG_SET_ORDER, 0);
        return $matches[0][1] ?? null;
    }
}

