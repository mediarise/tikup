@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card text-white badge-success mb-3">
                <div class="card-header">Платеж успешно получен!</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                        <p class="text-white"><a href="{{ route('cabinet.order') }}">Перейти к моим заказам</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
