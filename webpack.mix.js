const mix = require("laravel-mix");
const fs = require("fs");

// Расширеный конфиг для вебпака
mix.webpackConfig({
    optimization: {
        splitChunks: {
            chunks: "async",
            minSize: 30000,
            // maxSize: 0,
            minChunks: 1,
            maxAsyncRequests: 8,
            maxInitialRequests: 3,
            automaticNameDelimiter: "~",
            name: true,
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true
                }
            }
        }
    },
    resolve: {
        extensions: [".js", ".vue", ".json"],
        alias: {
            vue$: "vue/dist/vue.js",
            "~": __dirname + "/",
            "@": __dirname + "/resources/"
        }
    },
    plugins: [
        // new BundleAnalyzerPlugin(),
    ]
});

// Хелпер для работы с сингл файлами
let getFiles = function(dir) {
    return fs.readdirSync(dir).filter(file => {
        return fs.statSync(`${dir}/${file}`).isFile();
    });
};

//Хотрелоуд при изменении файлов
mix.browserSync({
    open: "external",
    host: process.env.APP_LOCAL,
    proxy: process.env.APP_LOCAL,
    files: [
        "resources/views/**/*.php",
        "app/**/*.php",
        "routes/**/*.php",
        "public/"
    ]
});

// Single file for client start
getFiles("resources/js/pages").forEach(function(filepath) {
    mix.js("resources/js/pages/" + filepath, "public/js/pages/");
});
/*
getFiles("resources/sass").forEach(function(filepath) {
    mix.sass("resources/sass/" + filepath, "public/css/");
});
*/

// Single file for client end

 mix.js("resources/js/app.js", "js/app.js")
 mix.js("resources/js/legacy/script.js", "js/script.js")
/*    .autoload({
        jquery: ["$", "jQuery"],
        "popper.js/dist/umd/popper.js": ["Popper"]
    })*/
    .sass("resources/sass/app.scss", "public/css")
  //  .sass('resources/sass/app.scss', 'public/css')
    .copyDirectory("resources/images", "public/images")
//    .copyDirectory("resources/vendor/ckeditor", "public/vendor/ckeditor");
if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps();
}
