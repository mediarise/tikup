<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>Быстрая накрутка в Тик Ток: лайки, реальные просмотры и Тик Ток подписчики</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description"
          content="Ракрутка в Тик Ток и попадание в топ рекомендации быстро и эффективно. Тик Ток реальные подписчики, просмотры и лайки в Тик Ток в сервисе раскрутки TikUp">
    <meta name="keywords" content="">

    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">
    <meta name="mailru-domain" content="4sAp2LsiZy9LMaXN"/>


    <meta property="og:title" content="Быстрая накрутка в Тик Ток: лайки, реальные просмотры и Тик Ток подписчики"/>
    <meta property="og:site_name" content="TikUp — Сервис накрутки подписчиков, лайков и просмотров в TikTok"/>
    <meta property="og:url" content="{{ url('/') }}"/>
    <meta property="og:description"
          content="Накрутка в Тик Ток и попадание в топ рекомендации быстро и эффективно. Тик Ток реальные подписчики, просмотры и лайки в Тик Ток в сервисе раскрутки TikUp"/>
    <meta property="og:image" content="{{ asset('images/social-network.jpg') }}"/>
    <meta property="twitter:title" content="Быстрая накрутка в Тик Ток: лайки, реальные просмотры и Тик Ток подписчики">
    <meta property="twitter:description"
          content="Накрутка в Тик Ток и попадание в топ рекомендации быстро и эффективно. Тик Ток реальные подписчики, просмотры и лайки в Тик Ток в сервисе раскрутки TikUp">


    <link rel="icon" type="image/png" href="/images/favicon.ico">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">


    <style>
        button.nav-link {
            width: 100%;
            cursor: pointer;
        }

    </style>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .btn-outline-light a {
            color: #f8f9fa;
            background-color: transparent;
            background-image: none;
            border-color: #f8f9fa;
        }

        .btn-outline-light a:hover {
            color: #d6619c;

        }

        .frame-outline {
            font-size: 12px;
            font-weight: 400;
            text-transform: uppercase;
            padding: 0.375rem 1.35rem;
            transition: all 0.3s ease;
            color: #fff;
            background-color: transparent;
            background-image: none;

            border: 1px solid #f8f9fa;
            /* padding: .375rem .75rem; */
            /* font-size: 1rem; */
            line-height: 1.5;
            border-radius: .25rem;
        }

        .frame-outline a {
            padding: 0 !important;
        }

        .frame-outline a,
        .frame-outline a:hover {
            color: #ffffff;
        }

        @media (min-width: 992px) {
            .ml-lg-3, .mx-lg-3 {
                margin-left: 1rem !important;
                margin-right: 1rem !important;
            }
        }
    </style>

</head>

<body data-spy="scroll" data-target="#navbar" data-offset="30">

<div class="nav-menu navbar-light" style="padding: 0;     background: rgba(60,8,118,.8);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-dark navbar-expand-lg" style="    justify-content: space-around;">

                    <div class="nav-left">
                        <a class="navbar-brand" href="{{ url('/') }}" title="{{ config('app.name', 'Laravel') }}"><img
                                src="/images/logo-tikup.png" alt="{{ config('app.name', 'Laravel') }}"/></a>


                        <a style="color: #ffffff" href="mailto:info@tikup.ru">
                            <svg class="bi bi-envelope" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M16 5H4a1 1 0 00-1 1v8a1 1 0 001 1h12a1 1 0 001-1V6a1 1 0 00-1-1zM4 4a2 2 0 00-2 2v8a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2H4z"
                                      clip-rule="evenodd"></path>
                                <path fill-rule="evenodd"
                                      d="M2.071 6.243a.5.5 0 01.686-.172L10 10.417l7.243-4.346a.5.5 0 11.514.858L10 11.583 2.243 6.93a.5.5 0 01-.172-.686z"
                                      clip-rule="evenodd"></path>
                            </svg>
                            info@tikup.ru</a>

                        <span style="padding: 0 10px; color: #ffffff"><i class="fab fa-telegram"></i> Telegram: <a
                                style="color: #ffffff" href="https://t.me/tikupru" target="_blank">@tikupru</a></span>

                    </div>
                    <div class="nav-right  "  >

                        <div class=" " id="navbar">
                            <ul class="navbar-nav  frame-outline">


                                <li class="nav-item  my-3 my-sm-0 ml-lg-3">
                                    @guest
                                        <a class="" href="{{ route('login') }}">{{ __('Войти') }}</a>
                                        @if (Route::has('register'))
                                            / <a class=""
                                                 href="{{ route('register') }}">{{ __('Зарегистрироваться') }}</a>
                                        @endif
                                    @else
                                        Добро пожаловать {{ Auth::user()->name }} <span class="caret"></span>
                                    @endguest
                                </li>
                                @guest
                                @else
                                    <li class="nav-item my-3 my-sm-0 ml-lg-3 ">
                                        <a href="{{ route('cabinet.order') }}">{{ __('Мой кабинет') }}</a>
                                    </li>
                                    <li class="nav-item my-3 my-sm-0 ml-lg-3 ">
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Выход') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                @endguest
                            </ul>
                        </div>

                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>


<header class="bg-gradient" id="home">
    <div class="container mt-5">
        <h1>Сервис TikUp</h1>
        <h6 style="font-size: 2rem; padding-bottom: 15px;">Накрутка подписчиков, лайков и просмотров в TikTok</h6>

        <p class="tagline">Главное преимущество сервиса — подписчики (фанаты), просмотры и лайки накручиваются с
            Российских аккаунтов,
            что напрямую влияет на попадание в топ именно для РФ и стран СНГ!</p>
    </div>

    <div style="padding: 30px 0 0 0;" class="col-md-12 text-center">
        <a href="#widget-form" class="btn btn-primary btn-lg">
            Заказать накрутку
        </a>
    </div>

    <div class="img-holder mt-3"><img src="images/iphonex.png" alt="phone" class="img-fluid"></div>
</header>

<div class="section light-bg" id="features" style="padding-bottom: 0;">

    <div class="container">
        <div class="section-title">
            <small>Факты</small>
            <h3>Наши преимущества</h3>
        </div>
        <div class="row" style="padding: 0 0 30px 0;">
            <div class="col-12 col-lg-4">
                <div class="card features">
                    <div class="card-body">
                        <div class="media">
                            <span class="ti-face-smile gradient-fill ti-3x mr-3"></span>
                            <div class="media-body">
                                <h4 class="card-title">Для чего покупать подписчиков TikTok?</h4>
                                <p class="card-text">Это быстрый способ увеличить свою аудиторию за счёт популярности
                                    твоего аккаунта.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <div class="card features">
                    <div class="card-body">
                        <div class="media">
                            <span class="ti-star gradient-fill ti-3x mr-3"></span>
                            <div class="media-body">
                                <h4 class="card-title">Стань популярным в TikTok</h4>
                                <p class="card-text">Наши клиенты успешно начали свою карьеру с социальных сетях. Вы
                                    станете следующим?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <div class="card features">
                    <div class="card-body">
                        <div class="media">
                            <span class="ti-lock gradient-fill ti-3x mr-3"></span>
                            <div class="media-body">
                                <h4 class="card-title">Безопасность накрутки TikTok</h4>
                                <p class="card-text">
                                    Ни один из наших клиентов не был заблокирован за использование наших услуг.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-4">
                <div class="card features">
                    <div class="card-body">
                        <div class="media">
                            <span class="ti-rocket gradient-fill ti-3x mr-3"></span>
                            <div class="media-body">
                                <h4 class="card-title">Успех гарантирован</h4>
                                <p class="card-text">Ты хочешь увидеть рост подписиков? TikUp гарантирует практически
                                    мгновенный топ</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card features">
                    <div class="card-body">
                        <div class="media">
                            <span class="ti-time gradient-fill ti-3x mr-3"></span>
                            <div class="media-body">
                                <h4 class="card-title">Быстрая доставка</h4>
                                <p class="card-text">
                                    Заказы обрабатываются в течении нескольких минут, а ты в это время отдыхай и
                                    наблюдай за результатом!
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card features">
                    <div class="card-body">
                        <div class="media">
                            <span class="ti-comments-smiley gradient-fill ti-3x mr-3"></span>
                            <div class="media-body">
                                <h4 class="card-title">Превосходный сервис</h4>
                                <p class="card-text">Мы очень серьезно относимся к контролю качества и гарантируем
                                    только положительный результат!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div style="padding: 30px 0 0 0;" class="col-md-12 text-center">
                <a href="#widget-form" class="btn btn-primary btn-lg">
                    Заказать накрутку
                </a>
            </div>
        </div>
    </div>


</div>


<div class="section" id="youtube" style="padding-bottom:0;">
    <div class="container">
        <div class="section-title">
            <small>Как работает сервис</small>
            <h3>Видеоинструкция</h3>
        </div>

        <div class="row" style="text-align: center;">
            <div class="col-12 col-lg-12">

                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/_pgccgQjxVI"></iframe>
                </div>
            </div>
        </div>
        <div class="row">
            <div style="padding: 30px 0 0 0;" class="col-md-12 text-center">
                <a href="#widget-form" class="btn btn-primary btn-lg">
                    Заказать накрутку
                </a>
            </div>
        </div>
    </div>


</div>


<div id="v-frontpage">
    <div class="loader">
        <i class="fa fa-cog fa-5x fa-spin"></i>
    </div>
</div>


<div class="section light-bg">

    <div class="container">

        <div class="section-title">
            <small>Возможности</small>
            <h3>Наши преимущества</h3>
        </div>

        <div class="row">


            <div class="col-md-8 d-flex align-items-center">


                <ul class="list-unstyled ui-steps">
                    <li class="media">
                        <div class="circle-icon mr-4">1</div>
                        <div class="media-body">
                            <h5>Автоматическая накрутка</h5>
                            <p>Заказы быстро обрабатываются автоматически и исполняются в течении нескольких минут.</p>
                        </div>
                    </li>
                    <li class="media my-4">
                        <div class="circle-icon mr-4">2</div>
                        <div class="media-body">
                            <h5>Быстрый результат</h5>
                            <p>Хотите увидеть рост своих пользователей? Посты в TikTok оцениваются по лайкам, просмотрам
                                и комментариям. С нашей системой вы быстро попадете в тренды.</p>
                        </div>
                    </li>
                    <li class="media">
                        <div class="circle-icon mr-4">3</div>
                        <div class="media-body">
                            <h5>Прирост аудитории</h5>
                            <p>Многие знаменитости в социальных сетях успешно начали свою карьеру, используя наши
                                услуги. Вы станете следующим?</p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <img src="images/iphonex.png" alt="iphone" class="img-fluid">
            </div>

        </div>

    </div>

</div>
<div class="section bg-gradient">
    <div class="container">
        <div class="call-to-action">

            <div class="box-icon"><span class="ti-mobile gradient-fill ti-3x"></span></div>
            <h2>TikUp - сервис для накрутки лайков, подписчиков и просмотров в социальной сети TikTok</h2>
            <p class="tagline">Накрутить подписчиков, лайки и просмотры в Тикток теперь быстро и просто! Большое
                количество просмотров на видео поможет вам выделиться и попасть в рекомендации ТикТок.</p>

        </div>
    </div>

</div>

<footer class="my-5 text-center">

    <p class="mb-2"><small>© 2020. ООО "МЕДИАРАЙС" ИНН: 5948057818 КПП: 594801001</small></p>

    <a href="/terms-of-use" target="_blank" class="m-3">Пользовательское соглашение</a>
    <br>
    <br>
    <p>Контактная информация</p>

    <a href="mailto:info@tikup.ru">
        <svg class="bi bi-envelope" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd"
                  d="M16 5H4a1 1 0 00-1 1v8a1 1 0 001 1h12a1 1 0 001-1V6a1 1 0 00-1-1zM4 4a2 2 0 00-2 2v8a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2H4z"
                  clip-rule="evenodd"></path>
            <path fill-rule="evenodd"
                  d="M2.071 6.243a.5.5 0 01.686-.172L10 10.417l7.243-4.346a.5.5 0 11.514.858L10 11.583 2.243 6.93a.5.5 0 01-.172-.686z"
                  clip-rule="evenodd"></path>
        </svg>
        info@tikup.ru</a>

</footer>


<script>
    window.app_data = {};
    const token = document.querySelector('meta[name="csrf-token"]')
    if (token) {
        window.app_data.token = token.content;
    } else {
        console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
    }

</script>

<script src="{{mix('js/app.js')}}"></script>
<script src="{{mix('js/script.js')}}"></script>

<script src="{{mix('js/pages/frontpage.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" defer></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(57182449, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true,
        ecommerce: "dataLayer"
    });
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/57182449" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

</body>

</html>
