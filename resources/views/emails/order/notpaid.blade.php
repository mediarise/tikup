@component('mail::message')

# Уважаемый {{ $name }},<br />

<p>Мы заметили, что у Вас возникли проблемы с оплатой.</p>
<p>Если Вам необходима помощь, пожалуйста напишите нам на почту <a href="mailto:support@tikup.ru">support@tikup.ru</a></p>

![](https://media.giphy.com/media/QVgU7wEY0RlV6/giphy.gif)

<p>Или попробуйте создать новый заказ</p>
@component('mail::button', ['url' => 'https://tikup.ru/#widget-form',  'color' => 'primary'])
Новый заказ
@endcomponent

С уважением,<br>
{{ config('app.name') }}

@endcomponent
