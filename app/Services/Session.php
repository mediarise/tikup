<?php

namespace App\Services;

use Illuminate\Http\Request;

/**
 * Class Session
 * @package App\Services
 */
class Session
{

    /**
     * @var Request
     */
    private Request $request;

    /**
     * Session constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param array $data
     */
    public function requestToSession(array $data)
    {
        array_walk($data, function ($current) {
            if (is_array($current)) {
                $this->request->session()->put($current);
            } else {
                $this->request->session()->put($current, $this->request->post($current));
            }
        });

    }

    /**
     * @param $key
     * @return mixed
     */
    public function getValueAndRemove($key)
    {
        $value = $this->request->get($key, null);
        if (empty($value) && $this->request->session()->has($key)) {
            $value = $this->request->session()->get($key);
            $this->request->session()->remove($key);
        }
        return $value;
    }


}
