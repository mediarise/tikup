<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Request;

/**
 * App\OrderLogs
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $255 Сообщение действия журнала
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLogs newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLogs newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLogs query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLogs where255($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLogs whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLogs whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLogs whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLogs whereUserId($value)
 * @mixin \Eloquent
 * @property string $message Сообщение действия журнала
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLogs whereMessage($value)
 * @property string $ip_user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLogs whereIpUser($value)
 */
class OrderLogs extends Model
{

    public static function log(String $message)
    {
        $user = Auth::check() ? Auth::user() : null;

        $orderLog = new OrderLogs();
        $orderLog->user_id = $user ? $user->id : null;
        $orderLog->message = $user ? 'Действие пользователя: ' . $user->id . ' email: ' . $user->email . ' ' . $message : $message;
        $orderLog->ip_user = Request::ip();

        return $orderLog->save();
    }
}
