@component('mail::message')

# Уважаемый {{ $name }},<br /> заказ № {{ $orderId }} выполнен

![](https://media.giphy.com/media/R3S6MfUoKvBVS/giphy.gif)

@component('mail::button', ['url' => 'https://tikup.ru/#widget-form',  'color' => 'primary'])
Новый заказ
@endcomponent

С уважением,<br>
{{ config('app.name') }}

@endcomponent
