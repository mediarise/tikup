<?php

namespace App;

interface InvoiceInterface
{
    const PAYMENT_SUCCEEDED = 'payment.succeeded';

    function applyPayment();
}
