const inputWrapper = {
    $_veeValidate: {
        name() {
            return this.name;
        },
        value() {
            return this.value;
        }
    },
    props: {
        labelText: String,
        labelTextSmall: {
            type: String,
            default: ''
        },
        disabled: {
            type: Boolean,
            default: false
        },
        error: {
            type: String,
            required: false
        },
        inputClass: {
            type: String,
            default: 'col-md-9'
        },
        type: {
            type: String,
            default: 'text',
            validator: val => {
                return ['url', 'text', 'password', 'email', 'search'].indexOf(val) !== -1;
            }
        },
        placeholder: {
            type: String,
            default: ''
        },
        name: String,
        value: {
            default: ''
        },
        info: {
            type: String,
            default: ''
        }
    },
    methods: {
        updateValue(e) {
            this.$emit('input', e.target.value);
        },
        updateSelect(val) {
            this.$emit('input', val);
            this.$emit('change', val);
        }
    }
};
export default inputWrapper;
