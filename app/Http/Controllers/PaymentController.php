<?php

namespace App\Http\Controllers;

use App\Helpers\GenUuid;
use App\Invoice;
use App\Order;
use App\OrderLogs;
use App\Price;
use App\Services\InvoiceService;
use App\Services\OrderService;
use App\Services\payments\YandexKassaNotification;
use App\Services\payments\YandexKassaProvider;

use App\Services\Session;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PaymentController extends Controller
{

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('payment');
    }

    /**
     * @param Request $request
     * @param Invoice $invoice
     * @param Order $order
     * @return RedirectResponse
     */
    public function make(Request $request, Invoice $invoice, Order $order)
    {
        InvoiceService::make($request, $invoice, $order);
        return redirect()->route('cabinet.order');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function checkout(Request $request)
    {
        OrderLogs::log('Пользователь начал checkout');
        $invoiceId = $request->get('invoice_id');
        $orderId = $request->get('order_id');

        if (empty($invoiceId) || empty($orderId)) {
            return redirect()->route('cabinet');
        }

        $invoice = InvoiceService::checkout($invoiceId);
        OrderService::checkout($orderId);

        try {
            $paymentUrl = $invoice->getPaymentUrl();
            return redirect()->to($paymentUrl);

        } catch (\Exception $e) {
            OrderLogs::log('Ошибка при оплате ' . $e->getMessage());
            abort(500);
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function callback(Request $request)
    {
        $operatorId = $request->get('operator_id');
        $inputData = json_decode($request->getContent());

        OrderLogs::log('Callback оператора: ' . $operatorId . ' данные: ' . json_encode($inputData));
        $notification = null;

        try {
            switch ($operatorId) {
                case Invoice::YANDEX_KASSA_ID:

                    $notification = new YandexKassaNotification();
                    $notification->event = $inputData->event;
                    $notification->type = $inputData->type;
                    $notification->object = $inputData->object;

                    $invoice = Invoice::where('payment_id', '=', $notification->object->id)->firstOrFail();
                    $notification->invoice = $invoice;

                    $provider = new YandexKassaProvider();
                    break;
                default:
                    $provider = null;
                    break;
            }

            if ($provider === null) throw new NotFoundHttpException();
            return $provider->handleRequest($notification);
        } catch (\Exception $e) {
            OrderLogs::log('Ошибка вызыва callback платежной системы ' . $operatorId . ' message: ' . $e->getMessage() . ' code: ' . $e->getCode() . ' line: ' . $e->getLine());
            abort(500);
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function confirm(Request $request)
    {
        $invoiceId = $request->get('invoice_id');
        $code = $request->get('code');
        $invoice = Invoice::findOrFail((int)$invoiceId);

        if (!$invoice->validatePayment($code)) {
            throw new BadRequestHttpException();
        }

        $statusPayment = $invoice->getPaymentProvider()->getPaymentStatus();
        OrderLogs::log("Статус платежа, который вернулся $statusPayment");

        /**
         * Логику полностью перенести в Invoice
         */
        if ($statusPayment === Invoice::STATUS_PAID) {
            $invoice->applyPayment();
            return redirect()->route('payment.success');
        } elseif ($statusPayment === Invoice::STATUS_FAIL) {
            $invoice->failPayment();
            return redirect()->route('payment.fail');
        } elseif ($statusPayment === Invoice::STATUS_CANCELED) {
            $invoice->canceledPayment();
            return redirect()->route('payment.canceled');
        } elseif ($statusPayment === Invoice::STATUS_WAITING_CAPTURE || $statusPayment === Invoice::STATUS_PENDING_PAY) {
            /**
             * Костыль
             */
            $invoice->pendingPayment();
            $paymentUrl = "https://money.yandex.ru/payments/checkout/confirmation?orderId=$invoice->payment_id";
            return redirect()->route('payment.pending', ['payment_url' => $paymentUrl]);
        }

        return redirect()->route('payment.fail');
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function success(Request $request)
    {
        return view('payment.success');
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function fail(Request $request)
    {
        return view('payment.fail');
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function canceled(Request $request)
    {
        return view('payment.canceled');
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function pending(Request $request)
    {
        $paymentUrl = $request->get('payment_url');
        return view('payment.pending', ['paymentUrl' => $paymentUrl]);
    }
}
