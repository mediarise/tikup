<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSendToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->smallInteger('send_success')->default(0)->comment('Отправка письма с выполенным заказом');
            $table->timestamp('send_success_at')->nullable()->comment('Дата отправки письма с успешшно выполненным заказом');
            $table->smallInteger('send_not_pay')->default(0)->comment('Отправка письма с просьбой оплаты');
            $table->timestamp('send_not_pay_at')->nullable()->comment('Дата отправки письма с просбюой оплатить');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('send_not_pay_at');
            $table->dropColumn('send_not_pay');
            $table->dropColumn('send_success_at');
            $table->dropColumn('send_success');
        });
    }
}
