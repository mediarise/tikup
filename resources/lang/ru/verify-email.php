<?php

return [
    'Verify Email Address' => 'Подтверждение email адреса',
    'Please click the button below to verify your email address.' => 'Пожалуйста, нажмите кнопку ниже, чтобы подтвердить свой адрес электронной почты.',
    'If you did not create an account, no further action is required.' => 'Если вы не создавали учетную запись, то проигнорируйте данное письмо',

];
