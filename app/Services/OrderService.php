<?php

namespace App\Services;

use App\Invoice;
use App\Order;
use App\OrderLogs;
use App\Price;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderService
{

    /**
     * @param Request $request
     * @param Invoice $invoice
     * @param Order $order
     * @return Order
     */
    public static function make(Request $request, Invoice $invoice, Order $order)
    {
        $link = $request->has('link') ? $request->post('link') : $request->session()->get('link');
        $link = trim($link);
        $order->invoice_id = $invoice->id;
        $order->status = Order::STATUS_NEW;
        $order->link = $invoice->price_id === Price::FOLLOWERS_ID ? Order::prepareLink($link) : $link;
        $order->user_id = Auth::user()->id;
        $order->image = $order->getImage();
        $order->charge = $invoice->value;

        if ($order->save()) {
            OrderLogs::log('Пользователь создал заказ: ' . $order->id);
        }

        $request->session()->has('link') ?: $request->session()->remove('link');
        return $order;
    }

    public static function checkout(int $orderId)
    {
        $order = Order::findOrFail($orderId);
        $order->status = Order::STATUS_PENDING_PAY;
        if ($order->save()) {
            OrderLogs::log('Заказ ' . $order->id . ' в статусе ожидания оплаты');
        }
        return $order;
    }
}
