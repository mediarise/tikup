<?php

namespace App\Services;

use App\Helpers\GenUuid;
use App\Invoice;
use App\Order;
use App\OrderLogs;
use App\Price;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class InvoiceService
{

    /**
     * @param Request $request
     * @param Invoice $invoice
     * @param Order $order
     * @return Invoice
     */
    public static function make(Request $request, Invoice $invoice, Order $order)
    {
        $value = $request->has('value') ? $request->post('value') : $request->session()->get('value');
        $priceId = $request->has('priceId') ? $request->post('priceId') : $request->session()->get('priceId');
        $link = $request->has('link') ? $request->post('link') : $request->session()->get('link');

        if (empty($value) || empty($priceId) || empty($link)) {
            return null;
        }

        OrderLogs::log('Пользователь хочет заказать ' .
            Price::TARIFF[$priceId]['TITLE']
            . ' количество ' . $value .
            ' по ссылке ' . $link);

        $invoice->status = Invoice::STATUS_NEW;
        $invoice->value = $value;
        $invoice->user_id = Auth::user()->id;
        $invoice->price_id = Price::TARIFF[$priceId]['ID'];
        $invoice->total = (function () use ($invoice, $priceId) {
            return round(Price::TARIFF[$priceId]['COST'] * $invoice->value, 2);
        })();

        if ($invoice->save()) OrderLogs::log('Пользователь создал инвойс: ' . $invoice->id);

        $request->session()->has('value') ?: $request->session()->remove('value');
        $request->session()->has('priceId') ?: $request->session()->remove('priceId');

        OrderService::make($request, $invoice, $order);

        $request->session()->has('value') ?: $request->session()->remove('value');
        $request->session()->has('priceId') ?: $request->session()->remove('priceId');

        return $invoice;
    }

    public static function checkout(int $invoiceId)
    {
        $invoice = Invoice::findOrFail($invoiceId);

        $invoice->status = Invoice::STATUS_PENDING_PAY;
        $invoice->payment_id = GenUuid::gen();
        $invoice->currency_id = Invoice::CURRENCY_RUB;
        $invoice->operator_pay_id = Invoice::YANDEX_KASSA_ID;
        $invoice->idempodence_key = GenUuid::gen();

        if ($invoice->save()) {
            OrderLogs::log('Инвойс ' . $invoice->id . ' в статусе ожидания оплаты');
        }

        return $invoice;
    }

}
