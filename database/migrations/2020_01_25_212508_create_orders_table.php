<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('id');
            $table->bigInteger('invoice_id')->unsigned();
            $table->index('invoice_id');
            $table->foreign('invoice_id')
                ->references('id')->on('invoices');
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->index('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->string('link')->comment('Ссылка на заказ');
            $table->smallInteger('status')->default(1)->comment('Статус заказа');
            $table->integer('order_id_api')->nullable()->comment('ID заказа в партнерской системе');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
