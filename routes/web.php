<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'SiteController@index')->name('homepage');
Route::get('terms-of-use', 'SiteController@termsOfUse');


Route::get('/cabinet', 'CabinetController@index')->name('cabinet');
Route::get('/payment', 'PaymentController@index')->name('payment');


Route::match(['GET', 'POST'], '/payment/make', 'PaymentController@make')->name('payment.make');
Route::get('/payment/checkout', 'PaymentController@checkout')->name('payment.checkout');
Route::get('/payment/confirm', 'PaymentController@confirm')->name('payment.confirm');
Route::get('/payment/success', 'PaymentController@success')->name('payment.success');
Route::get('/payment/fail', 'PaymentController@fail')->name('payment.fail');
Route::get('/payment/canceled', 'PaymentController@canceled')->name('payment.canceled');
Route::get('/payment/pending', 'PaymentController@pending')->name('payment.pending');
Route::get('/cabinet/order', 'CabinetController@order')->name('cabinet.order');

Auth::routes(['verify' => true]);
Route::get('/payment/callback', 'PaymentController@callback')->name('payment.callback');
