<?php

namespace App\Console\Commands;

use App\Invoice;
use App\Order;
use App\OrderLogs;
use Illuminate\Console\Command;

class OrderCheckPay extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:check-pay';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запускает обработку оплаченных заказов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $orders = Order::with('invoice')
            ->where('status', Order::STATUS_PENDING_PAY)
            ->limit(100)->distinct()->get()->all();

        if (empty($orders)) {
            return 1;
        }

        $countOrders = count($orders);
        OrderLogs::log("Начинаем проверку платежей пользователей, найдено не оплаченных заказов со статусом ожидания оплаты: " . $countOrders);

        try {
            array_map(function (Order $order) {

                if (!empty($order->invoice->operator_pay_id)) {

                    $statusPayment = $order->invoice->getPaymentProvider()->getPaymentStatus();

                    /**
                     * Логику полностью перенести в Invoice
                     */
                    if ($statusPayment === Invoice::STATUS_PAID) {
                        $order->invoice->applyPayment();
                        OrderLogs::log("Платеж принят заказа клиента $order->user->email оператором оплаты: $order->invoice->operator_pay_id");
                    } elseif ($statusPayment === Invoice::STATUS_FAIL) {
                        $order->invoice->failPayment();
                        OrderLogs::log("Ошибка оплаты заказа клиента $order->user->email оператором оплаты: $order->invoice->operator_pay_id");
                    } elseif ($statusPayment === Invoice::STATUS_CANCELED) {
                        $order->invoice->canceledPayment();
                        OrderLogs::log("Отмена оплаты заказа клиента $order->user->email оператором оплаты: $order->invoice->operator_pay_id");
                    } elseif ($statusPayment === Invoice::STATUS_WAITING_CAPTURE || $statusPayment === Invoice::STATUS_PENDING_PAY) {
                        $order->invoice->pendingPayment();
                        OrderLogs::log("Ожидание оплаты заказа клиента $order->user->email оператором оплаты: $order->invoice->operator_pay_id");
                    }
                }
            }, $orders);
            return 0;
        } catch (\Exception $e) {
            var_dump('exception');
            var_dump($e->getLine() . ' ' . $e->getMessage() . ' ' . $e->getFile());
            return 1;
        }
    }
}
