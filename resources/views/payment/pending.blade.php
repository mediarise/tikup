@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card   badge-warning mb-3">
                    <div class="card-header">Ожидание оплаты</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <p style="color: #000000">Ваш заказ ожидает оплаты, если вы случайно здесь оказались, можете
                            перейти к оплате по
                            ссылке:</p>
                        <a class="btn btn-primary" href="{{ url($paymentUrl) }}">Перейти к оплате</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
