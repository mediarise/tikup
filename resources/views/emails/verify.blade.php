@component('mail::message')
# @lang('Добро пожаловать в TikUp')

Пожалуйста подтвердите почту
@component('mail::button', ['url' => $url,  'color' => 'primary'])
Подтвердить
@endcomponent


С уважением,<br>
{{ config('app.name') }}


@slot('subcopy')
@lang(
"Если у вас возникли проблемы с нажатием кнопки \":actionText\", тогда скопируйте и вставьте URL ниже\n".
'в ваш веб-браузер: [:actionURL](:actionURL)', ['actionText' => 'Подтвердить', 'actionURL' => $url])
@endslot

@endcomponent
