<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTypeMessageToOrderLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_logs', function (Blueprint $table) {
            $table->dropColumn('message');
        });
        Schema::table('order_logs', function (Blueprint $table) {
            $table->text('message')->comment('Сообщение действия журнала');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_logs', function (Blueprint $table) {
            $table->dropColumn('message');
        });

        Schema::table('order_logs', function (Blueprint $table) {
            $table->string('message', 255)->comment('Сообщение действия журнала');
        });
    }
}
