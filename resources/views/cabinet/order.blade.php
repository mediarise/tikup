@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="card">
                    <div class="card-header">Мои заказы</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="alert alert-success" role="alert">
                            Перед заказом убедитесь, что Ваш аккаунт публичный (открыт для всех пользователей).<br/>
                            Накрутка начнётся через пару минут, после оплаченного заказа.
                        </div>
                        <div class="alert alert-warning" role="alert">
                            <b>Внимание!</b> Если у Вас не получилось оплатить или вы из другой страны, можете написать
                            в поддержку: <a href="mailto: {{ \Config::get('app.support_email') }} "> {{ \Config::get('app.support_email') }}</a>, мы решим проблему
                        </div>

                        <div class="form-group" style="padding: 20px 0 0 0">
                            <a href="{{route('homepage','#widget-form')}}" class="btn btn-primary">Новый заказ</a>
                            <a href="{{ route('cabinet.order') }}" class="btn btn-primary">Обновить</a>
                        </div>

                        <div class="row">
                            @if (empty($orders))
                                <p>Нет заказов</p>
                            @endif
                            @foreach ($orders as $order)

                                <div class="card col-12 col-md-3 p-0" style="margin: 8px;">
                                    <div
                                        style="width: auto; height: 250px; background: url({{$order->image}}); background-position: 50% 50%;
                                            background-size: cover;
                                            background-repeat: no-repeat;"></div>
                                    <div class="card-body" style="padding: 20px 0 0 0">

                                        <ul class="list-group list-group-flush text-center">
                                            <li class="list-group-item">
                                                <p class="card-text">{{ \App\Price::PRICES[$order->invoice->price_id] }}</p>
                                            </li>
                                            <li class="list-group-item">
                                                <p class="card-text">Номер заказа: № {{$order->id}}  </p>
                                            </li>
                                            <li class="list-group-item ">

                                                <p class="card-text">Статус:
                                                    {!!\App\Order::STATUS_TITLE[$order->status]!!}
                                                    @if ((int)$order->status === \App\Order::STATUS_NEW)
                                                        <a href="{{route('payment.checkout', ['order_id'=> $order->id, 'invoice_id' => $order->invoice_id])}}"
                                                           class="btn btn-primary btn-sm"
                                                           style="display: block; padding: 5px">Оплатить</a>
                                                    @endif
                                                    @if ((int)$order->status === \App\Order::STATUS_FAIL_PAID)
                                                        Если Вы оплатили, но вышла данная ошибка, просим срочно
                                                        написать в
                                                        поддержку <a href="mailto:{{ Config::get('app.support_email') }} ">{{ Config::get('app.support_email') }}</a>
                                                    @endif
                                                </p>
                                            </li>
                                            <li class="list-group-item">
                                                <p class="card-text">{{$order->link}}</p>
                                            </li>

                                            <li class="list-group-item">
                                                <p class="card-text">
                                                    Выполнено: {{$order->invoice->value - $order->charge}}
                                                    из {{$order->invoice->value}}</p>
                                                <div class="progress">
                                                    <div class="progress-bar bg-success" role="progressbar"
                                                         style="width: {{ ($order->invoice->value - $order->charge) * 100 /  $order->invoice->value}}%"
                                                         aria-valuenow="{{ ($order->invoice->value - $order->charge) * 100 /  $order->invoice->value}}"
                                                         aria-valuemin="0"
                                                         aria-valuemax="100"></div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <p class="card-text">Стоимость {{$order->invoice->total}} ₽</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <!-- <td>{{ $order->invoice->payment_at ? date('d.m.Y H:i', strtotime($order->invoice->payment_at)) : null}}</td> -->
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
