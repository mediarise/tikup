<?php

namespace App\Services\payments;

class PaymentConfirmation
{
    public $paymentId = null;
    public $confirmationUrl = null;
}
