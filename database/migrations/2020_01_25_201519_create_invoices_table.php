<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->index('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->decimal('total', 9, 2)->comment('Всего');
            $table->smallInteger('price_id')->default(1)->comment('ID прайса');
            $table->integer('value')->comment('Количество заказа');
            $table->timestamp('payment_at')->nullable()->comment('Дата и время платежа');
            $table->smallInteger('status')->default(1)->comment('Статус платежа');
            $table->string('payment_id')->nullable()->comment('ID Платежа в платежной системе');
            $table->index('payment_id');
            $table->text('description')->nullable()->comment('Описание платежа');
            $table->integer('operator_pay_id')->nullable()->comment('Платежная система');
            $table->index('operator_pay_id');
            $table->text('tax_response')->nullable()->comment('Ответ системы чеков');
            $table->integer('currency_id')->nullable()->comment('Валюта');
            $table->string('idempodence_key', 127)->nullable()->comment('Ключ идемпотентности');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
