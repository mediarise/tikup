<?php

namespace App\Http\Middleware;

use App\OrderLogs;
use App\Services\Session;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class PaymentMake
{

    /**
     * @param $request
     * @param Closure $next
     * @param null $guard
     * @return RedirectResponse|mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if ($request->is('payment/make') && !Auth::guard($guard)->check()) {
            (new Session($request))->requestToSession([['redirect' => 'payment.make'], 'value', 'link', 'priceId']);
            OrderLogs::log('Пользователь не авторизован, отправлен на регистрацию');
            return redirect()->route('register');
        }

        return $next($request);
    }

}
