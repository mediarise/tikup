<?php

namespace App;

use App\Services\PrepareLink;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Order
 *
 * @property int $id
 * @property int $invoice_id
 * @property int $user_id
 * @property string $link Ссылка на заказ
 * @property int $status Статус заказа
 * @property int $order_id_api ID заказа в партнерской системе
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereOrderIdApi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUserId($value)
 * @property string $image Изображение контента
 * @property int $charge количество выполненного заказа
 * @mixin \Eloquent
 * @property int $send_success Отправка письма с выполенным заказом
 * @property string|null $send_success_at Дата отправки письма с успешшно выполненным заказом
 * @property int $send_not_pay Отправка письма с просьбой оплаты
 * @property string|null $send_not_pay_at Дата отправки письма с просбюой оплатить
 * @property-read \App\Invoice $invoice
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereSendNotPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereSendNotPayAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereSendSuccess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereSendSuccessAt($value)
 */
class Order extends Model
{
    const STATUS_NEW = 1;
    const STATUS_PAID = 2;
    const STATUS_PENDING = 3;
    const STATUS_IN_PROGRESS = 4;
    const STATUS_COMPLETED = 5;
    const STATUS_FAIL_PAID = 6;
    const STATUS_REJECTED = 0;
    const STATUS_PENDING_PAY = 7;
    const STATUS_ERROR = 8;

    const SEND_SUCCESS_SENDED = 1;
    const SEND_SUCCESS_NOT_SENDED = 0;
    const SEND_NOT_PAY_SENDED = 1;
    const SEND_NOT_PAY_NOT_SENDED = 0;

    const STATUS_TITLE = [
        self::STATUS_NEW => '<span class="badge badge-light" style="font-size: 100%">Новый (не оплачен)</span>',
        self::STATUS_PAID => '<span class="badge badge-primary" style="font-size: 100%">Оплачен</span>',
        self::STATUS_PENDING => '<span class="badge badge-info" style="font-size: 100%; color: #ffffff;">Ожидает обработки</span>',
        self::STATUS_IN_PROGRESS => '<span class="badge badge-warning" style="font-size: 100%">В процессе выполнения</span>',
        self::STATUS_COMPLETED => '<span class="badge badge-success" style="font-size: 100%">Выполненный</span>',
        self::STATUS_FAIL_PAID => '<span class="badge badge-secondary" style="font-size: 100%">Ошибка оплаты</span>',
        self::STATUS_REJECTED => '<span class="badge badge-danger" style="font-size: 100%">Обработка в ручном режиме</span>',
        self::STATUS_PENDING_PAY => '<span class="badge badge-warning" style="font-size: 100%">В процессе оплаты</span>',
    ];


    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public static function prepareLink($link)
    {
        $urlTikTok = 'https://www.tiktok.com/@';

        $link = trim($link);

        $re = '/@([^\/]+)/';
        preg_match($re, $link, $matches);
        if (!empty($matches)) {
            return $urlTikTok . end($matches);
        }

        return $urlTikTok . $link;
    }


    public function getImage()
    {
        $client = new Client();
        $prepareLink = new PrepareLink($this->link, $client);

        if ((int)$this->invoice->price_id === Price::FOLLOWERS_ID) {
            return $prepareLink->getAvatar();
        }
        return $prepareLink->getVideoPreview();
    }

}
